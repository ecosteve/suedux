import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:suedux/color.dart';
import 'package:suedux/consts.dart';
import 'package:suedux/models/scan_event.dart';
import 'package:suedux/redux/actions/scan_flow.dart';

import 'package:suedux/redux/app_state.dart';
import 'package:suedux/redux/states/scan_flow.dart';
import 'package:suedux/redux/store.dart';

import 'package:async_redux/async_redux.dart' as Redux;
import 'package:suedux/services/scan.dart';
import 'package:suedux/type.dart';
import 'package:suedux/utils.dart';


class _Dashboard extends StatefulWidget {

  final ScanFlowStateEnum state;
  final List<ScanEvent> scanEvents;

  _Dashboard({
    required this.state,
    required this.scanEvents
  });

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<_Dashboard> {

  void scanOut() async {
    store.dispatch(UpdateScanFlowState(value: ScanFlowStateEnum.IDLE));
    ScanService.scanOut();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

     body: Padding(padding: EdgeInsets.only(top: kGutterWidth), child:
      Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(padding: EdgeInsets.only(top:kGutterWidth, left:kGutterWidth), child:
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.end,
              children:[
                Flexible(flex: 0, child: Text("Good ${Utils.flatulation} ", style: TextStyles.subtitle(context, PigColor.headlineText , FontWeight.bold))),
            ]),
          ),

          Padding(padding: EdgeInsets.symmetric(horizontal: kGutterWidth), child:
            Text("Let's get started", style: TextStyles.regular(context)),
          ),

          SizedBox(height: 20),

          // APP IS IDLE
          if (store.state.scanFlowState.flowState == ScanFlowStateEnum.IDLE)
            Padding(padding: EdgeInsets.symmetric(horizontal: kGutterWidth), child:
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  OutlinedButton(

                    onPressed: () {
                      Navigator.of(context).pushNamed("/scan");
                    },
                    child: Text('Tap to begin.... ')
                  ),

                  Text('Idle.. yawn', style: TextStyles.subtitle(context)),
                ])
            ),


          // APP IS SCANNED IN
          if (store.state.scanFlowState.flowState == ScanFlowStateEnum.SCANNED_IN)
            Padding(padding: EdgeInsets.symmetric(horizontal: kGutterWidth), child:
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [

                OutlinedButton(
                  onPressed: scanOut,
                  child: Text('Tap here to scan out')
                ),

                Text('You are scanned in!', style: TextStyles.subtitle(context)),
              ])
            ),


            Flexible(child:
              Padding(padding: EdgeInsets.symmetric(horizontal: kGutterWidth), child:
                ListView.builder(
                  itemCount: widget.scanEvents.length,
                  itemBuilder: (context, idx) {
                    ScanEvent event = widget.scanEvents[idx];
                    return Row(children: [
                      Text(DateFormat('jm').format(event.timestamp), style: TextStyles.important(context)),
                      Container(width: 5),
                      Text(event.typeAsString, style: TextStyles.regular(context))
                    ]);
                  })
            )
          )
        ])
      )
    );
  }
}


class DashboardModel extends Redux.BaseModel<AppState> {

  DashboardModel();

  late ScanFlowStateEnum flowState;
  late List<ScanEvent> scanEvents;

  DashboardModel.build({
    required this.flowState,
    required this.scanEvents,
  }) : super(equals: [flowState, scanEvents]);

  @override
  DashboardModel fromStore() {
    return DashboardModel.build(
      flowState: state.scanFlowState.flowState,
      scanEvents: state.scanEventState.events
    );
  }
}

class Dashboard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Redux.StoreConnector<AppState, DashboardModel>(
      model: DashboardModel(),
      builder: (BuildContext context, DashboardModel vm) => _Dashboard(
        state: vm.flowState,
        scanEvents: vm.scanEvents
      ),
    );
  }
}