import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:suedux/color.dart';
import 'package:suedux/mixins/dialog.dart';
import 'package:suedux/redux/actions/scan_flow.dart';
import 'package:suedux/redux/states/scan_flow.dart';
import 'package:suedux/redux/store.dart';
import 'package:suedux/services/scan.dart';
import 'package:suedux/type.dart';
import 'package:suedux/widgets/qr_scanner.dart';

class Scan extends StatefulWidget {
  @override
  State<Scan> createState() => _ScanState();
}

class _ScanState extends State<Scan> with DialogBuilder {

  void handleScan(Barcode? result, ScanFlowStateEnum state) async {
    if (result == null) return;

    await ScanService.decodeScan(result);

    await store.dispatchAsync(UpdateScanFlowState(value: state));
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(actions: [
        TextButton(onPressed: () => Navigator.of(context).pop, child: Text('Back'))
      ]),
      body: Column(
        children: [
          Container(
            height: 350,
            width: MediaQuery.of(context).size.width,
            child: Stack(children: [
              Container(child: QRScanner(onComplete: (res) => handleScan(res, ScanFlowStateEnum.SCANNED_IN)))
            ])
          ),
          Container(
            height: 40.0, width: MediaQuery.of(context).size.width,
            color: PigColor.interfaceGrey,
            padding: EdgeInsets.only(top: 10),
              child:
              Text("Scan any QR code", style: TextStyles.regular(context), textAlign: TextAlign.center),
          ),
        ],
      )
    );
  }
}

