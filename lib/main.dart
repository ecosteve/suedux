import 'package:async_redux/async_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:suedux/pages/dashboard.dart';
import 'package:suedux/pages/scan.dart';
import 'package:suedux/redux/app_state.dart';
import 'package:suedux/redux/store.dart';

void main() {
  runApp(App());
}

class App extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) => StoreProvider<AppState>(
    store: store,
    child: MaterialApp(
      title: 'Sues Dope Redux App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        //visualDensity: VisualDensity.adaptivePlatformDensity,
        backgroundColor: Colors.white,
        scaffoldBackgroundColor: Colors.white
      ),
      initialRoute: '/',
      routes: {
        '/dashboard': (context) => Dashboard(),
        '/scan': (context) => Scan(),
      },
      home: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
          statusBarColor: Colors.black
        ),
        child: Dashboard()),
    )
  );
}

