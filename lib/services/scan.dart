
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:suedux/models/scan_event.dart';
import 'package:suedux/redux/actions/scan_event.dart';
import 'package:suedux/redux/store.dart';
import 'package:uuid/uuid.dart';

class ScanService  {

  /// picks apart the scan data, upserts site and generates event
  static decodeScan(Barcode scan) async {
    try {
      ScanEvent event = ScanEvent(id: Uuid().v4(), timestamp: DateTime.now(), type: ScanEventType.SCAN_IN, data: ScanEventData(
        qrCodeData: scan
      ));
      // upsert event
      await store.dispatchAsync(AddEvent(event: event));
    } catch (e) {
      return e;
    }
  }


  static scanOut() async {

    await store.dispatchAsync(AddEvent(event: ScanEvent(
      id: Uuid().v4(),
      type: ScanEventType.SCAN_OUT,
      timestamp: DateTime.now(),
      data: ScanEventData(
        qrCodeData: null
      )
    )));
  }

}