import 'package:suedux/models/scan_event.dart';
import 'package:suedux/redux/app_state.dart';
import 'package:suedux/redux/base_action.dart';
import 'package:suedux/redux/states/scan_event.dart';

class AddEvent extends BaseAction {

  final ScanEvent event;

  AddEvent({ required this.event });

  @override
  Future<AppState> reduce() async {

    List<ScanEvent> events = List.of(state.scanEventState.events);

    await ScanEvent.db.insert(event);
    events.insert(0, event);

    return state.copy(scanEventState: ScanEventState(events: events));

  }
}