
import 'package:suedux/redux/app_state.dart';
import 'package:suedux/redux/base_action.dart';
import 'package:suedux/redux/states/scan_flow.dart';

class UpdateScanFlowState extends BaseAction {
  final ScanFlowStateEnum value;

  UpdateScanFlowState({ required this.value });

  @override
  Future<AppState> reduce() async {

    ScanFlowState scanFlowState = ScanFlowState(flowState: value);

    return state.copy(scanFlowState: scanFlowState);
  }
}