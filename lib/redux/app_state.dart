
import 'package:suedux/redux/states/scan_event.dart';
import 'package:suedux/redux/states/scan_flow.dart';

class AppState {
  final ScanEventState scanEventState;
  final ScanFlowState scanFlowState;

  AppState({
    required this.scanEventState,
    required this.scanFlowState
  });

  AppState copy({
    ScanEventState? scanEventState,
    ScanFlowState? scanFlowState
  }) {
    return AppState(
      scanEventState: scanEventState ?? this.scanEventState,
      scanFlowState: scanFlowState ?? this.scanFlowState
    );
  }

  static AppState initialState() => AppState(
    scanEventState: ScanEventState.initialState(),
    scanFlowState: ScanFlowState.initialState()
  );

  @override
  bool operator ==(Object other) => (
    identical(this, other) ||
      other is AppState &&
      runtimeType == other.runtimeType &&
      scanEventState == other.scanEventState &&
      scanFlowState == other.scanFlowState
  );

  @override
  int get hashCode =>
    scanEventState.hashCode ^
    scanFlowState.hashCode;
}
