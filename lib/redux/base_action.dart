import 'package:async_redux/async_redux.dart';
import 'package:suedux/redux/app_state.dart';
import 'package:suedux/redux/states/scan_flow.dart';
import 'package:suedux/redux/states/scan_event.dart';

abstract class BaseAction extends ReduxAction<AppState> {
  // States
  ScanEventState get eventState => state.scanEventState;
  ScanFlowState get scanFlowState => state.scanFlowState;
}
