import 'package:flutter/foundation.dart';
import 'package:suedux/models/scan_event.dart';

class ScanEventState {
  List<ScanEvent> get events => ScanEvent.db.items;
  ScanEvent get latest {
    List<ScanEvent> sorted = List.of(ScanEvent.db.items);
    sorted.sort((l, r) { return l.timestamp.compareTo(r.timestamp); });
    return sorted.last;
  }

  ScanEventState({ List<ScanEvent> events = const [] }) {
    ScanEvent.db.replace(events);
  }

  ScanEventState copy({List<ScanEvent>? events}) {
    return ScanEventState(events: events ?? this.events);
  }

  static ScanEventState initialState() => ScanEventState(events: const []);

  Future persist() async {
    await ScanEvent.db.upsertMany(events);
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
      other is ScanEventState &&
      runtimeType == other.runtimeType &&
      listEquals(events, other.events);
  }

  @override
  int get hashCode => events.hashCode;
}
