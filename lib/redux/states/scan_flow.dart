
enum ScanFlowStateEnum { IDLE, SCANNED_IN }

class ScanFlowState {

  ScanFlowStateEnum flowState;

  ScanFlowState({
    this.flowState = ScanFlowStateEnum.IDLE
  });

  ScanFlowState copy({ ScanFlowStateEnum? state }) {
    return ScanFlowState(flowState: state ?? this.flowState);
  }

  static ScanFlowState initialState() => ScanFlowState(flowState: ScanFlowStateEnum.IDLE);

  void forceState(ScanFlowStateEnum newState) => this.flowState = newState;

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
      other is ScanFlowState &&
      runtimeType == other.runtimeType &&
      flowState == other.flowState;
  }

  @override
  int get hashCode => flowState.hashCode;
}
