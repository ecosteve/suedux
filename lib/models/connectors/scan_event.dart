


import 'package:suedux/database.dart';
import 'package:suedux/models/connectors/connector.dart';
import 'package:suedux/models/scan_event.dart';

class ScanEventConnector extends DatabaseConnector<ScanEvent> {
  String get tableName => 'scan';

  List<ScanEvent> get events => items;

  @override
  ScanEvent parseRecord(Map<String, dynamic> record) {
    return ScanEvent.fromMap(record);
  }

  @override
  Future<List<ScanEvent>> load() async {
    var db = await DBProvider.db;

    var result = await db.query(tableName);
    List<ScanEvent> items = result.map(parseRecord).toList();
    replace(items);
    return items;
  }

  Future upsert(ScanEvent record) async {
    await destroyAll();
    await insert(record);

    return replace(await load());
  }

}
