

import 'dart:convert';

import 'package:suedux/models/connectors/scan_event.dart';
import 'package:suedux/redux/states/scan_flow.dart';
import 'package:suedux/redux/store.dart' as Store;
import 'package:suedux/models/base.dart';
import 'package:uuid/uuid.dart';

var uuid = new Uuid();

enum ScanEventType { SCAN_IN, SCAN_OUT }

class ScanEvent implements BaseModel {

  String id;
  ScanEventType type;
  DateTime timestamp;
  ScanEventData data;

  static ScanEventConnector db = ScanEventConnector();

  ScanEvent({
    required this.id,
    required this.type,
    required this.timestamp,
    required this.data
  });

  static ScanFlowState get store => Store.store.state.scanFlowState;

  String get typeAsString => type == ScanEventType.SCAN_IN ? "Scan In" : "Scan Out";

  factory ScanEvent.fromMap(Map<String, dynamic> map) {
    return ScanEvent(
      id: map['id'] ?? uuid.v4(),
      type: ScanEventType.values[int.parse(map['type'])],
      timestamp: DateTime.parse(map['timestamp']),
      data: ScanEventData.fromMap(json.decode(map['data']))
    );
  }

  @override
  Map<String, dynamic> toMap() => {
    "id": id,
    "type": type.index,
    "timestamp": timestamp.toIso8601String(),
    "data": json.encode(data.toMap())
  };

}

class ScanEventData {

  String? qrCodeData;

  ScanEventData({
    qrCodeData
   });

  factory ScanEventData.fromMap(Map<String, dynamic> map) {
    return ScanEventData(
      qrCodeData: map['qr_code_data']
    );
  }

  Map<String, dynamic> toMap() => {
    "qr_code_data": qrCodeData
  };

}

