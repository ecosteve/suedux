
const kGutterWidth        = 30.0;
const kInputPadding       = 15.0;
const kBorderRadius       = 5.0;
const kBottomAppBarHeight = 85.0;
